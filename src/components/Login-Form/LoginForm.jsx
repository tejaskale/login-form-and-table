import React, { Fragment, useState } from "react";
import Table from "../Table/Table.jsx";
import "./LoginForm-styles.css";

function LoginForm() {
  let [alldata, setalldata] = useState([]);
  let [name, setName] = useState("");
  let [email, setMail] = useState("");
  let [profession, setProfession] = useState("");
  let [phoneNum, setNum] = useState("");
  let [hobby, setHobby] = useState("");
  let [city, setCity] = useState("");

  function handleClickOnSave() {
    let data = {
      name: "",
      email: "",
      profession: "",
      phoneNum: "",
      hobby: "",
      city: "",
    };
    data.name = name;
    data.email = email;
    data.profession = profession;
    data.phoneNum = phoneNum;
    data.city = city;
    setalldata([...alldata, data]);
    console.log(alldata);

    setName("");
    setMail("");
    setProfession(!profession.checked);
    setHobby("");
    setNum("");

    // var city = document.getElementById('city')
    // city.firstChild.selected = true
  }

  return (
    <Fragment>
      <div className="wrapper">
        <div className="container">
          <div className="inp-container">
            <p className="inp-info-text">Full Name</p>
            <input
              type="text"
              onChange={(e) => {
                setName(e.target.value);
              }}
              value={name}
              className="text-type-inp"
            />
          </div>

          <div className="inp-container">
            <p className="inp-info-text">Email Address</p>
            <input
              type="email"
              onChange={(e) => {
                setMail(e.target.value);
              }}
              value={email}
              className="text-type-inp"
            />
          </div>

          <div className="inp-container">
            <p className="inp-info-text">Phone Number</p>
            <input
              type="number"
              onChange={(e) => {
                setNum(e.target.value);
              }}
              value={phoneNum}
              className="text-type-inp"
            />
          </div>

          <div className="inp-container">
            <p className="inp-info-text">Profession</p>
            <input
              type="radio"
              onChange={(e) => {
                setProfession(e.target.value);
              }}
              className="prfn-inp"
              name="profession"
              value="Student"
              id="Student"
            />{" "}
            <label htmlFor="Student">Student</label>
            <input
              type="radio"
              onChange={(e) => {
                setProfession(e.target.value);
              }}
              className="prfn-inp"
              name="profession"
              value="working"
              id="working"
            />{" "}
            <label htmlFor="working">working</label>
          </div>

          <div className="inp-container">
            <label htmlFor="City">Select Your City</label>
            <select
              name="City"
              id="city"
              className="city-select-tag"
              onChange={(e) => {
                setCity(e.target.value);
              }}
            >
              <option value="" disabled selected>
                Options
              </option>
              <option value="Ahmedabad">Ahmedabad</option>
              <option value="Gowtham">Gowtham</option>
              <option value="Arkham">Arkham</option>
              <option value="Goa">Goa</option>
            </select>
          </div>

          <div className="inp-btn-container">
            <button className="btn" onClick={handleClickOnSave}>
              Save
            </button>
          </div>
        </div>

        <Table
          alldata={alldata}
          setalldata={setalldata}
          setProfession={setProfession}
          setCity={setCity}
          setName={setName}
          setMail={setMail}
          setNum={setNum}
        />
      </div>
    </Fragment>
  );
}

export default LoginForm;
